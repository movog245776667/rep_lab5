# Определение функции для умножения
def multiply(a, b):
    return a * b

# Пример использования функции
num1 = 5
num2 = 3
result = multiply(num1, num2)
print(f"Результат умножения {num1} на {num2} равен {result}")